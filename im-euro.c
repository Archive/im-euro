/* GTK - The GIMP Toolkit
 * Copyright (C) 2000 Red Hat Software
 * Copyright (C) 2002 Yusuke Tabata
 * Copyright (C) 2004 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Owen Taylor <otaylor@redhat.com>
 *          Yusuke Tabata <tee@kuis.kyoto-u.ac.jp>
 *          Yukihiro Nakai <ynakai@redhat.com>
 *
 */

#include <string.h>

#include <gdk/gdkkeysyms.h>

#include <gtk/gtk.h>
#include <gtk/gtkimmodule.h>

#define IM_CONTEXT_EURO(obj) (GTK_CHECK_CAST((obj), type_euro, IMContextEuro))
#define BUFSIZ 1024

typedef struct _IMContextEuro {
  GtkIMContext parent;
  gboolean euro_mode_on;
  gchar* preedit_buf; /* Hiragana string as much as possible */

  GtkWidget* modewin;
  GtkWidget* modelabel;

  GdkWindow* client_window;
} IMContextEuro;

typedef struct _IMContextEuroClass {
  GtkIMContextClass parent_class;
} IMContextEuroClass;

typedef struct _ConvTable {
  gchar* spell;
  gchar* ideo;
} ConvTable;

ConvTable convtable[] = {
  { "dollar", "$" },
  { "euro", "€" }, /* U+20ac */
};

GType type_euro = 0;

static void im_euro_class_init (GtkIMContextClass *class);
static void im_euro_init (GtkIMContext *im_context);
static void im_euro_finalize(GObject *obj);
static void im_euro_set_client_window(GtkIMContext* context, GdkWindow* win);
static gboolean im_euro_filter_keypress(GtkIMContext *context,
		                GdkEventKey *key);
static void im_euro_get_preedit_string(GtkIMContext *, gchar **str,
					PangoAttrList **attrs,
					gint *cursor_pos);
static gboolean im_euro_is_modechangekey(GtkIMContext *context, GdkEventKey *key);
static void im_euro_update_modewin(IMContextEuro* ec);

static void
im_euro_register_type (GTypeModule *module)
{
  static const GTypeInfo object_info =
  {
    sizeof (IMContextEuroClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) im_euro_class_init,
    NULL,           /* class_finalize */
    NULL,           /* class_data */
    sizeof (IMContextEuro),
    0,
    (GtkObjectInitFunc) im_euro_init,
  };

  type_euro = 
    g_type_module_register_type (module,
				 GTK_TYPE_IM_CONTEXT,
				 "IMContextEuro",
				 &object_info, 0);
}

static void
im_euro_class_init (GtkIMContextClass *class)
{
  GtkIMContextClass *im_context_class = GTK_IM_CONTEXT_CLASS(class);
  GObjectClass *object_class = G_OBJECT_CLASS(class);

  im_context_class->set_client_window = im_euro_set_client_window;
  im_context_class->filter_keypress = im_euro_filter_keypress;
  im_context_class->get_preedit_string = im_euro_get_preedit_string;

  object_class->finalize = im_euro_finalize;
}

static void
im_euro_init (GtkIMContext *im_context)
{
  IMContextEuro *ec = IM_CONTEXT_EURO(im_context);
  ec->preedit_buf = g_new0(gchar, BUFSIZ);
  ec->euro_mode_on = FALSE;

  ec->modewin = gtk_window_new(GTK_WINDOW_POPUP);
  ec->modelabel = gtk_label_new("Euro");
  gtk_container_set_border_width(GTK_CONTAINER(ec->modewin), 5);
  gtk_container_add(GTK_CONTAINER(ec->modewin), ec->modelabel);

  ec->client_window = NULL;
}

static const GtkIMContextInfo im_euro_info = { 
  "Euro",		   /* ID */
  "Euro",             /* Human readable name */
  "gtk+",			   /* Translation domain */
   "",		   /* Dir for bindtextdomain (not strictly needed for "gtk+") */
   ""			           /* Languages for which this module is the default */
};

static const GtkIMContextInfo *info_list[] = {
  &im_euro_info
};

void
im_module_init (GTypeModule *module)
{
  im_euro_register_type(module);
}

void 
im_module_exit (void)
{
}

void 
im_module_list (const GtkIMContextInfo ***contexts,
		int                      *n_contexts)
{
  *contexts = info_list;
  *n_contexts = G_N_ELEMENTS (info_list);
}

GtkIMContext *
im_module_create (const gchar *context_id)
{
  if (strcmp (context_id, "Euro") == 0)
    return GTK_IM_CONTEXT (g_object_new (type_euro, NULL));
  else
    return NULL;
}

gboolean
im_euro_filter_keypress(GtkIMContext *context, GdkEventKey *key)
{
  IMContextEuro *ec = IM_CONTEXT_EURO(context);

  if( key->type == GDK_KEY_RELEASE ) {
    return FALSE;
  };

  if( im_euro_is_modechangekey(context, key) ) {
    ec->euro_mode_on = !ec->euro_mode_on;
    if( !ec->euro_mode_on && *ec->preedit_buf != '\0' ) {
     g_signal_emit_by_name(ec, "commit", ec->preedit_buf);
     memset(ec->preedit_buf, 0, BUFSIZ);
     g_signal_emit_by_name(ec, "preedit_changed");
    }
    im_euro_update_modewin(ec);
    return TRUE;
  }
  im_euro_update_modewin(ec);

  if( !ec->euro_mode_on ) {
    gunichar keyinput = gdk_keyval_to_unicode(key->keyval);
    gchar ubuf[7];
    /* Gdk handles unknown key as 0x0000 */
    if( keyinput == 0 )
      return FALSE;
                                                                                
    if( key->state & GDK_CONTROL_MASK )
      return FALSE;
                                                                                
    /* For real char keys */
    memset(ubuf, 0, 7);
    g_unichar_to_utf8(keyinput, ubuf);
    g_signal_emit_by_name(ec, "commit", ubuf);
    return TRUE;
  }

  if( key->keyval == GDK_space && *ec->preedit_buf != '\0' ) {
    guint i = 0;
    for(i=0;i<G_N_ELEMENTS(convtable);i++) {
      /* For the sake of maintainability */
      gchar* spell = convtable[i].spell;
      gchar* ideo = convtable[i].ideo;
      gchar* p = ec->preedit_buf;

      /* spell:"dollar" -> ideo:"$" */
      if( (strlen(p) >= strlen(spell))
          && !strcmp(spell, p + strlen(p)-strlen(spell)) ) {
        gchar* newbuf = g_new0(gchar, strlen(p)-strlen(spell)+strlen(ideo)+1);
        strncpy(newbuf, p,  strlen(p)-strlen(spell));
        strcat(newbuf, ideo);
        strcpy(p, newbuf);
        g_free(newbuf);
        g_signal_emit_by_name(ec, "preedit_changed");
        return TRUE;
      }

      /* ideo:"$" -> spell:"dollar" */
      if( (strlen(p) >= strlen(ideo))
          && !strcmp(ideo, p + strlen(p) - strlen(ideo)) ) {
        gchar* newbuf = g_new0(gchar, strlen(p)-strlen(ideo)+strlen(spell)+1);
        strncpy(newbuf, p,  strlen(p)-strlen(ideo));
        strcat(newbuf, spell);
        strcpy(p, newbuf);
        g_free(newbuf);
        g_signal_emit_by_name(ec, "preedit_changed");
        return TRUE;
      }
      
    }
    return TRUE;
  }

  if( key->keyval == GDK_Return && *ec->preedit_buf != '\0' ) {
    g_signal_emit_by_name(ec, "commit", ec->preedit_buf);
    memset(ec->preedit_buf, 0, BUFSIZ);
    g_signal_emit_by_name(ec, "preedit_changed");
    return TRUE;
  }

  if( key->keyval < GDK_a && key->keyval > GDK_z ) {
    reteurn FALSE;
  }

  if( strlen(ec->preedit_buf) == BUFSIZ )
    return FALSE;

  ec->preedit_buf[strlen(ec->preedit_buf)+1] = '\0';
  ec->preedit_buf[strlen(ec->preedit_buf)] = gdk_keyval_to_unicode(key->keyval);
  g_signal_emit_by_name(ec, "preedit_changed");

  return FALSE;
}

void
im_euro_get_preedit_string(GtkIMContext *ic, gchar **str,
			    PangoAttrList **attrs, gint *cursor_pos)
{
  IMContextEuro *ec = IM_CONTEXT_EURO(ic);
  if (attrs) {
    *attrs = pango_attr_list_new();
  }
  if (cursor_pos) {
    *cursor_pos = 0;
  }
  *str = g_strdup(ec->preedit_buf);
  *cursor_pos = strlen(*str);

  if( *str == NULL || strlen(*str) == 0 ) return;

  if (attrs) {
    PangoAttribute *attr;
    attr = pango_attr_underline_new(PANGO_UNDERLINE_SINGLE);
    attr->start_index = 0;
    attr->end_index = strlen(*str);
    pango_attr_list_insert(*attrs, attr);
  }
}

static gboolean
im_euro_is_modechangekey(GtkIMContext *context, GdkEventKey *key) {
  /* Kinput2 style - Shift + Space */
  if( key->state & GDK_SHIFT_MASK && key->keyval == GDK_space ) {
    return TRUE;
  /* Chinese/Korean style - Control + Space */
  } else if( key->state & GDK_CONTROL_MASK && key->keyval == GDK_space ) {     return TRUE;
  /* Windows Style - Alt + Kanji */
  } else if( key->keyval == GDK_Kanji ) {
    return TRUE;
  /* Egg Style - Control + '\' */
  } else if ( key->state & GDK_CONTROL_MASK && key->keyval == GDK_backslash ) {
    return TRUE;
  }
  /* or should be customizable with dialog */
                                                                                
  return FALSE;
}

static void
im_euro_finalize(GObject *obj) {
  IMContextEuro* ec = IM_CONTEXT_EURO(obj);
  gtk_widget_destroy(ec->modelabel);
  gtk_widget_destroy(ec->modewin);
}

static void
im_euro_move_modewin(IMContextEuro* ec) {
  GdkWindow* toplevel_gdk = ec->client_window;
  GdkScreen* screen;
  GdkWindow* root_window;
  GtkWidget* toplevel;
  GdkRectangle rect;
  GtkRequisition requisition;
  gint y;
  gint height;

  if( !toplevel_gdk )
    return;

  screen = gdk_drawable_get_screen (toplevel_gdk);
  root_window = gdk_screen_get_root_window(screen);

  while(TRUE) {
    GdkWindow* parent = gdk_window_get_parent(toplevel_gdk);
    if( parent == root_window )
      break;
    else
      toplevel_gdk = parent;
  }

  gdk_window_get_user_data(toplevel_gdk, (gpointer*)&toplevel);
  if( !toplevel )
    return;

  height = gdk_screen_get_height (gtk_widget_get_screen (toplevel));
  gdk_window_get_frame_extents (toplevel->window, &rect);
  gtk_widget_size_request(ec->modewin, &requisition);

  if (rect.y + rect.height + requisition.height < height)
    y = rect.y + rect.height;
  else
    y = height - requisition.height;

  gtk_window_move(GTK_WINDOW(ec->modewin), rect.x, y);
}

static void
im_euro_update_modewin(IMContextEuro* ec) {
  if( !ec->euro_mode_on ) {
    gtk_widget_hide_all(ec->modewin);
    return;
  } else {
    gtk_widget_show_all(ec->modewin);
    im_euro_move_modewin(ec);
  }
}

static void
im_euro_set_client_window(GtkIMContext* context, GdkWindow* win) {
  IMContextEuro* ec = (IMContextEuro*)context;

  ec->client_window = win;
}
